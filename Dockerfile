FROM node:6.10.3-alpine
COPY src/* /
WORKDIR /
EXPOSE 80
RUN ["node", "app.js", "build", "1507152575"]
CMD ["node", "app.js"]
